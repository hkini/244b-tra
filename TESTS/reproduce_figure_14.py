import os
import shutil
import time
import subprocess


# For figure 13. Might be interesting!

LINUX_SOURCE_PATH = 'LINUX_SOURCE_PATH/'

PARTIAL_TREE_UPDATE = True

REMOTE_IP = None # '172.20.10.8'
REMOTE_PORT = None
REMOTE_DIR = '~/TEST_SERVER_DIR'

def download_linux_source():
    if not(os.path.exists(LINUX_SOURCE_PATH)) or not(os.path.isdir(LINUX_SOURCE_PATH)):
        pass

def delete_and_recreate_empty_folder(folderpath):
    if os.path.exists(folderpath):
        shutil.rmtree(folderpath)
    os.mkdir(folderpath)


# Creates the test directories for figure 14.
'''
Specifically,
when we want an N-megabyte file tree we use a balanced
binary tree of height log_{2} N with N leaf directories,
each containing 256 four-kilobyte files with random binary
contents.
'''
POWERS_OF_TWO = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536]

def create_random_files(curr_path, file_index):
        for i in range(256):
            new_file_name = str(file_index + i)
            new_file_path = os.path.join(curr_path, new_file_name)
            with open(new_file_path, 'wb') as f:
                f.write(os.urandom(4096))

# Walks down the specified file tree, updating all 4kb files.
def update_files(curr_path):
    for file in os.listdir(curr_path):
        new_path = os.path.join(curr_path, file)
        if (new_path.endswith('.tracore_pickle')):
            continue
        if (os.path.isfile(new_path)):
            os.remove(new_path)
            print "Updating file " + new_path
            with open(new_path, 'wb') as f:
                f.write(os.urandom(4096))
        else:
            update_files(new_path)

# Walks down the specified file tree, updating just one subdirectory.
def update_files_partially(curr_path):
    for file in os.listdir(curr_path):
        new_path = os.path.join(curr_path, file)
        if os.path.isdir(new_path):
            print("updating inwards " + new_path)
            update_files_partially(new_path)
            return
        else:
            continue
    # We should only get here if we're in a folder with no subdirectories
    for file in os.listdir(curr_path):
        new_path = os.path.join(curr_path, file)
        if file.endswith('.tracore_pickle'):
            continue
        elif (os.path.isfile(new_path)):
            os.remove(new_path)
            print "updating file " + new_path
            with open(new_path, 'wb') as f:
                f.write(os.urandom(4096))
    return
            

file_index = 0
def create_test_directories(path, size_in_MB):
    global file_index
    file_index = 0
    # Figure out how many levels deep our directories need to be.
    levels_deep = 0
    for level, size in enumerate(POWERS_OF_TWO):
        if size_in_MB <= size:
            levels_deep = level
            break

    step = 1

    def recursively_create_subdirectories(curr_path, level, step):
        global file_index
        if (level <= 0):
            create_random_files(curr_path, file_index)
            file_index = file_index + 256
        else:
            for i in range(2):
                new_step = step + i
                new_folder_path = os.path.join(curr_path, str(new_step))
                os.mkdir(new_folder_path)
                recursively_create_subdirectories(new_folder_path, level - 1, new_step * 2)

    top_level_folder = os.path.join(path, str(step))
    delete_and_recreate_empty_folder(top_level_folder)
    step = step + 1

    recursively_create_subdirectories(top_level_folder, levels_deep, step)

# Spawns a server process to sync from client to server.
def run_server():
    server_args = ['python', '../trasrv_server_test.py', '--uuid', '5678']
    print("Server running: " + " ".join(server_args))
    return subprocess.Popen(server_args, shell=False)

# Spawns a client process to sync from client to server.
def run_client(server_path=None, client_path=None):
    client_args = ['python', '../tra_run_sync.py', client_path, server_path]
    if (REMOTE_IP != None):
        client_args.append('--host')
        client_args.append(REMOTE_IP)
    if (REMOTE_PORT != None):
        client_args.append('--port')
        client_args.append(str(REMOTE_PORT))
    print("Client running: " + " ".join(client_args))
    return subprocess.call(client_args, shell=False)

# Assumes this path exists, creates all subpaths.
def sync_all_directories_changed(path, size):
    # Set up all directories and files.
    client_test_dir = os.path.join(path, 'TEST_CLIENT_DIR')
    delete_and_recreate_empty_folder(client_test_dir)
    time.sleep(1)
    create_test_directories(client_test_dir, size)
    print("Created client dirs.")
    time.sleep(1)
    server_test_dir = REMOTE_DIR
    if (not(REMOTE_IP)):
        server_test_dir = os.path.join(path, 'TEST_SERVER_DIR')
        delete_and_recreate_empty_folder(server_test_dir)
        print("Created server dir.")
        
    time.sleep(1)


    # Sync over initial files so we are good to go on vector syncing
    # In theory this should work, but for some reason, I'm still seeing conflicts
    server_proc = run_server()
    run_client(server_path=server_test_dir, client_path=client_test_dir)
    print("Ran server and synced over initial files.")
    time.sleep(3)

    # Update either all files or just one leaf directory, based on the flag.
    if PARTIAL_TREE_UPDATE:
        update_files_partially(client_test_dir)
    else:
        update_files(client_test_dir)
    print("Updated client files.")
    time.sleep(5)

    # Run and time the sync. 
    before_time = time.time()
    run_client(server_path=server_test_dir, client_path=client_test_dir)
    after_time = time.time()
    print("Time taken for " + str(size) + " megabytes: " + str(after_time - before_time))
    server_proc.kill()


def run_all_directories_changed():
    for size in POWERS_OF_TWO[3:4]:
        sync_all_directories_changed(os.path.abspath('.'), size)

run_all_directories_changed()
