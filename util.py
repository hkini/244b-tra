import os
import time
import constant
import uuid

ROOT_PATH = 'TEST_DIR'

TRACORE_FILE = '.tracore_pickle'

UUID = None

class traVectorPair:
    def __init__(self):
        #  TODO why is this a dict ?
        self.modVector = {}
        self.syncVector = {}
        self.conflictedSyncVector = {}
        self.conflictedModVector = {}
        self.conflictedTimestamp = 0
        # Creation time vector determines if two files were created independently
        self.creationVector = {}
        
# TODO: Improve to logical clocks 
# For server
# cur_time = max(source.writestamp, logicalclock) 
# For client
# cur_time = max(logicalclock+1, systemtime)
# For now, use systemtime to get ball rolling
def get_current_time():
	return int(time.time())
	
def set_uuid(uuid):
    global UUID
    UUID = uuid

def get_uuid(path=ROOT_PATH):
    global UUID
    if UUID != None:
        return UUID
    uuid_path = constant.UUID_PATH
    uuid_path = os.path.abspath(os.path.expanduser(uuid_path))
    # If the UUID doesn't already exist in a file,
    # create it.
    if not(os.path.exists(uuid_path)):
        with open(uuid_path, 'w') as f:
            line = str(uuid.getnode())
            f.write(line)
    uuid_int = 0
    with open(uuid_path, 'r') as f:
        line = f.readline()
        uuid_int = int(line)
        UUID = uuid_int
    return uuid_int
    
# This appends source to request_vector
def copyVector(request_vector, source):
    if (constant.VERBOSE): print request_vector
    if (constant.VERBOSE): print source
    for replica in source:
        v = request_vector.add()
        v.replica = replica
        v.timestamp = source[replica]
        
# This overrides request_vector with source vector
def setVector(request_vector, source):
    #if (constant.VERBOSE): print request_vector
    #if (constant.VERBOSE): print source
    request_vector.clear()
    for replica in source:
        request_vector[replica] = source[replica]
        
def updateVectorForReplica(replica, source):
	cur_time = get_current_time()
	if (constant.VERBOSE): print "Update vector for " + str(replica) + " with latest timestamp " + str(cur_time)
	source[replica] = cur_time
	if (constant.VERBOSE): print source

def print_vector(vector):
    print "sync = {}".format(vector.syncVector)
    print "mod = {}".format(vector.modVector)
    print "creat = {}".format(vector.creationVector)

def remove_all_pickle(path):
    if os.path.isfile(os.path.join(path, ".tracore_pickle")):
        os.remove(os.path.join(path, ".tracore_pickle"))
    for p in os.listdir(path):
        new_path = os.path.join(path, p)
        if os.path.isdir(new_path):
            remove_all_pickle(new_path)

# Returns the union of the element wise max of two vectors s_A, s_B 
def max_vector(s_A, s_B):
    # Get all keys
    keys_A = s_A.keys()
    keys_B = s_B.keys()
    keys = list(set().union(keys_A, keys_B))
    s_C = {}
    for s in keys:
        if s in s_B and s in s_A:
            if s_A[s] <= s_B[s]:
            	s_C[s] = s_B[s]
            else:
            	s_C[s] = s_A[s]
        elif s in s_B and not s in s_A:
            s_C[s] = s_B[s]
        else:
            s_C[s] = s_A[s]
    return s_C

# This function creates a traVectorPair for the remote modVector and syncVector.
# TODO: Implement the optimization where "[t]he last element of the file's modification
# history sequence determines the result of the comparison."
# For now, take the latest timestamp from each replica.
def create_traVectorPair(modVector, syncVector, creationVector={}):
    vector = traVectorPair()
    for m in modVector:
        if m.replica in vector.modVector.keys() and m.timestamp < vector.modVector[m.replica]:
            continue
        vector.modVector[m.replica] = m.timestamp

    for s in syncVector:
        if s.replica in vector.syncVector.keys() and s.timestamp < vector.syncVector[s.replica]:
        	continue
        vector.syncVector[s.replica] = s.timestamp
	
    if not creationVector:
        vector.creationVector = {}        	
    else:
        vector.creationVector = {creationVector.replica: creationVector.timestamp}
    return vector


#remove_all_pickle("TEST_CLIENT_DIR/new_test")
#remove_all_pickle("TEST_SERVER_DIR/new_test")
