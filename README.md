# README #

This README would normally document whatever steps are necessary to get your application up and running.

We based our implementation off of the paper MIT_CSAIL-TR-2005-014.pdf. 

Symbols:
"paper" := MIT_CSAIL-TR-2005-014.pdf.
A ~> B := sync from A to B

### What is this repository for? ###

* Quick summary
This repositary implement a Tra server and client. The server should be run
 on the device where the file/directory is syncing to; the client should be run
 on the device where the file/directory is syncing from.

 The implementation is based on [tra paper]. It supports both single file and
 directory syncing. gRPC is used as the communication channel between client and
 server.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Instalation ###

- Dependency Requirements -
python 2.7, gRPC, protobuf3
- gRPC -
1. You may need to install gRPC: 
$ python -m pip install grpcio
This will install gRPC for python and protobuf3
3. Compile protobuf:
$ python -m grpc_tools.protoc -Iproto --grpc_python_out=. --python_out=. proto/trasrv.proto

- pip -
If you do not have pip, install pip by:
1. Download get-pip.py from http://pip.readthedocs.io/en/stable/installing/#install-pip
2. Run the script 
$ sudo python get-pip.py 
3. Install tools
$ sudo python -m pip install grpcio-tools
4. Create proto buf files
$ python -m grpc_tools.protoc --proto_path=proto/ --python_out=. --grpc_python_out=. proto/*.proto

### Quick Start ###
- Starting a server -
run python trasrv_server_test.py
Note that the server can be in run in any directory.
The default gRPC port 50051 will be used. Otherwise, user can specify the port with
option --port [port number]

- Syncing a directory to a server -
run python tra_run_sync.py [options] LOCAL_DIR_NAME REMOTE_DIR_NAME [
Note that REMOTE_DIR_NAME must be the full path at remote device. If user root (~/)
is used in the path, the path must be quoted. Tra will create the base directory
if it doesn't exist at remote.

- Resolving conflict -
When a tra server detect conflicts, it will store the conflicted file as
<file_name>.tra_conflict_<replica_id>_<last_mod_ts>. To resolve a conflict,
run python tra_resolve_conflict.py [conflict solution file] to select the version
that will be stored as the conflict solution.

### Implementation Overview ###

- Files Overview -
tra_core.py:
This file contains the core tra algorithm for vector time pair syncing.
Each directory use a traCore object to keep track of its vector time pair and
its files' vector time pairs. The traCore class handle all syncing decisions.

tra_fs.py:
This file handles interaction between meta data in traCore and the underlying
file system, including creating, serializing/deserailizing and updating the modification
time for a traCore.

tra_client.py:
This file contains a traClientJob that will use gRPC to send out sync request
for a directory to a server. For a directory sync, it will walk down the
directory tree following BFS. It will not recurse down to the children if a given
parent directory is synced.

proto/trasrv.proto:
protobuf definition for the rpcs.

### Testing Overview ###
Below describes our testing topology and cases we covered. 
Testing was done with --verbose on both client and server side.

- Conflict Resolution Test -
Graph shows sequence of changes as time progresses 1,2,3,4 from 3 different 
replicas A, B, C. Each letter, X, Y, Z, W denotes different versions of the file. 
If there is no file at time=t, it means file doesn't exist (yet). Copy 
would copy file over if file doesn't exist previously.

   A  B  C
1     X
      | \
2     Y  Z
    / | /
3  W  ? 
    \ |
4     ?

@ t=3, if we resolved in favor of Y, there is no conflict at 4. 

@ t=3, if we resolved in favor of Z, there is a conflict at 4. 

This is per paper https://pdos.csail.mit.edu/archive/6.824-2004/papers/tra.pdf
in section 3.4 Conflict Resolution. 

-  Sync Deletion - 
We ran test on Figure 4 (a)(b) as below:
NOTE: DEL means file is deleted.

   A  B  
1  X
   |  \
2  X   X
   |   |
3  DEL X
   |  /
4  DEL

A deletes file. Sync B ~> A at 4 will "Do Nothing"

   A  B  
1  X
   |  \
2  X   X
   |   |
3  X   DEL
   |  /
4  DEL

B deletes file. Sync B ~> A at 4 will "DELETE" file X

   A  B  
1  X
   |  \
2  X   X
   |   |
3  DEL Y
   |  /
4  CONFLICT!

@ t=3, A deletes file while B changes file. Sync B ~> A at 4 will "reports a conflict, 
since neither version is de- rived from the other."

-  Sync Copy - 
For the rest of the test cases, we used the same following setup.

   A  B  
1  X
   |  \
2  X   X
   |   |
3  X   Y
   |  /
4  Y

A copies X file over to B because there is no history of the file.
Sync B ~> A @ t=3 will take B's Y version because that's newer.

Setup: 
Client has the directory structure. 
	TEST_CLIENT_DIR/dir1/dir2/c.txt
	TEST_CLIENT_DIR/dir1/dir3/c3.txt
	TEST_CLIENT_DIR/dir1/c1.txt
Client IP: 10.0.1.2

Server has the directory structure: 
	TEST_SERVER_DIR
Server IP: 10.0.1.3	

Command line: 
On Server: 
$ rm -rf TEST_CLIENT_DIR/dir1; mkdir TEST_CLIENT_DIR/dir1; mkdir TEST_CLIENT_DIR/dir1/dir2; touch TEST_CLIENT_DIR/dir1/dir2/c.txt; mkdir TEST_CLIENT_DIR/dir1/dir3; touch TEST_CLIENT_DIR/dir1/c1.txt; rm -rf TEST_SERVER_DIR/dir1; rm -rf TEST_SERVER2_DIR/dir1 
$ python -m grpc_tools.protoc --proto_path=proto/ --python_out=. --grpc_python_out=. proto/*.proto
$ python trasrv_server_test.py --verbose

On Client:
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2

Action:
Client syncs TEST_CLIENT_DIR/dir1 to Server's TEST_SERVER_DIR/dir1 (using server's 
absolute path).

Command line: 
On Client:
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2

Result:
Server TEST_SERVER_DIR/dir1 has all files/directories from under TEST_CLIENT_DIR/dir1.

Example Server log:
	TRA_CORE: Found file c.txt
	Sync deletion or new file, A = {12345678L: 1513025797}
	A's vectors
	sync = {12345678L: 1513025797}
	mod = {12345678L: 1513025797}
	creat = {12345678L: 1513025797}
	B's vectors
	sync = {}
	mod = {12345678L: 1513025797}
	creat = {12345678L: 1513025797}
	Creation vector: {12345678 : 1513025797 } 
	sync_deletion: (independent) {12345678L: 1513025797} > {}
	Copy


- Sync Do Nothing - 

   A  B  
1  X
   |  \
2  X   X
   |   |
3  X   Y
   |  /
4  Y   Y
   |  /
5  Do Nothing

Action: 
Client issues the same sync again on TEST_CLIENT_DIR/dir1 to Server's TEST_SERVER_DIR/dir1.

Command line: python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2

Result: 
Server A returned sync result "Do Nothing". 

Example Server log:
	SYNC_DIR: DO nothing dir dir1
	TRA_SERVER: start job 1
	Done syncing at time 1512977813.31
	Done dumping at time 1512977813.31
	Status: Done with job 1
	TRA_SERVER: done with job 1, ts = 1512977813.31

- Partially Synced Directories - 

If a directory is partially synced, Tra will recurse down the directory. Otherwise, it 
will skip it. 

Action: 
To test this, client changed c.txt. 

Result: 
Server returned sync result "Do Nothing" for dir3 and "Copy" for dir2, 
which c.txt is under. 

Example Server log:
	SYNC_DIR: DO nothing dir dir3
 	SYNC_DIR: start syncing dir /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1/dir2 
	SYNC_DIR: COPY dir dir2
	SYNC_FILE: syncing c.txt
	...

- Sync Conflict - 

Action: 
To test this, client changed c.txt and server also changed c.txt. Client syncs to server. 

Command line: 
1. On Client: 
$ echo "Test Client changed" >> TEST_CLIENT_DIR/dir1/dir2/c.txt 

2. On Server: 
$ echo "Test Server changed" >> TEST_SERVER_DIR/dir1/dir2/c.txt 

3. On Client: 
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2

Result: 
Conflict is detected. Conflicted file is written to server with .tra_conflict_replicaID_timestamp.
_
Example Server log:
	SYNC_FILE: syncing c.txt
	A client c.txt B server 
	A's vectors
	sync = {12345678L: 1513025974}
	mod = {12345678L: 1513025974}
	creat = {12345678L: 1513025797}
	B's vectors
	sync = {189698557624974L: 1513025913, 12345678L: 1513025900, 9876543: 1513025956}
	mod = {12345678L: 1513025900, 9876543: 1513025956}
	creat = {12345678L: 1513025797}
	compare_tra_vectors (A->B): (mB) {12345678L: 1513025900, 9876543: 1513025956} ? (sA) {12345678L: 1513025974}
	Detected a conflict for c.txt

- Conflict Resolution -

Command line:
(To resolve conflict in favor of client.)
$ python tra_resolve_conflict.py /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1/dir2/c.txt.tra_conflict_12345678_1513025974 

- Conflict Resolution Saved -
Client makes changes to the file by deleting it. Client should take those changes 
since conflict was resolved in favor of client per conflict resolution example above.

Command line:
On Client:
$ rm -rf /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1/dir2/c.txt
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2

Result:
Server's file c.txt should also have deleted.

Example Server log:
	TRA_CORE: Missing file c.txt, dir dir2
	Sync deletion or new file, A = {12345678L: 1513025797}
	A's vectors
	sync = {189698557624974L: 1513025913, 12345678L: 1513025974, 9876543: 1513025956}
	mod = {12345678L: 1513025974}
	creat = {12345678L: 1513025797}
	B's vectors
	sync = {12345678L: 1513025998}
	mod = {}
	creat = {}
	Creation vector: {12345678 : 1513025797 } 
	sync_deletion: (m_A <= s_B) {12345678L: 1513025974} <= {12345678L: 1513025998}
	Client has already deleted the Server's file. Server will DELETE

- Figure 3 (page 4) of paper-
We also tested Figure 3 (page 4) of paper from class syllabus for illustration of test 
scenario. 

Setup: 
B has the directory structure. 
	TEST_CLIENT_DIR/dir1/dir2/c.txt
	TEST_CLIENT_DIR/dir1/dir3/c3.txt
	TEST_CLIENT_DIR/dir1/c1.txt
B IP: 10.0.1.3

A, C has the directory structure: 
	TEST_SERVER_DIR
A IP: 10.0.1.2
C IP: 10.0.1.4

-- Test case Figure 3(b) --
On A,B,C's server: 
$ python trasrv_server_test.py  --verbose

@ t=1: B ~> A
On B's client:
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2
@ t=2: B ~> C
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.4

@ t=3: A and B both changed file
On A's: 
$ echo "Test server A changed" >> TEST_SERVER_DIR/dir1/dir2/c.txt 

On B's:
$ echo "Test server B changed" >> TEST_CLIENT_DIR/dir1/dir2/c.txt 

On A's client: A ~> B
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=4: Conflict resolved in favor of A
$ python tra_resolve_conflict.py TEST_CLIENT_DIR/dir1/dir2/c.txt.tra_conflict_9876543_1513028400 

@ t=5: C ~>B
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=6: A changed file again and on A's client: A ~> B
$ echo "Test server A changed again" >> TEST_SERVER_DIR/dir1/dir2/c.txt 
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=7, A's changes from t=6 are copied over to B.

-- Test case Figure 3(c) --
On A,B,C's server: 
$ python trasrv_server_test.py  --verbose

@ t=1: B ~> A
On B's client:
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.2
@ t=2: B ~> C
$ python tra_run_sync.py TEST_CLIENT_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_SERVER_DIR/dir1  --verbose --host 10.0.1.4

@ t=3: A and B both changed file
On A's: 
$ echo "Test server A changed" >> TEST_SERVER_DIR/dir1/dir2/c.txt 

On B's:
$ echo "Test server B changed" >> TEST_CLIENT_DIR/dir1/dir2/c.txt 

On A's client: A ~> B
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=4: Conflict resolved in favor of B
$ python tra_resolve_conflict.py TEST_CLIENT_DIR/dir1/dir2/c.txt

On A's client: A ~> B does nothing because conflict was already resolved in B's favor
and A hasn't changed since conflict resolution. 
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=5: C ~>B does nothing 
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=6: A changed file again and on A's client: A ~> B
$ echo "Test server A changed again" >> TEST_SERVER_DIR/dir1/dir2/c.txt 
$ python tra_run_sync.py TEST_SERVER_DIR/dir1 /Volumes/Sources/cs244b/244b-tra/TEST_CLIENT_DIR/dir1 --host 10.0.1.3 --verbose

@ t=7, B reports conflict from A's changes @ t=6. 


- Known Bugs -
1. If a file is change do a directory with the same name or vice versa, the
server will attempt to create a file/directory while the old directory/file is still
on the system and fail.

* Repo owner or admin
* Other community or team contact
