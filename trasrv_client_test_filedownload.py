import grpc
import trasrv_pb2
import trasrv_pb2_grpc

def download_a_file(path='dir1/c.txt'):
    channel = grpc.insecure_channel('localhost:50051')
    stub = trasrv_pb2_grpc.traServerStub(channel)
    file_request = trasrv_pb2.fileRequest(path=path)

    
    responseGen = stub.download(file_request)
    response = responseGen.next()
    status = response.status
    bytes = response.message
    print(status)

    outfile = None
    if (status > 0):
        outfile = open('download_test_file.output', 'wb') 

    while (status > 0):
        outfile.write(bytes)
        response = responseGen.next()
        status = response.status
        bytes = response.message
        
    if outfile != None:
        outfile.close()
