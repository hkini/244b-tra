import grpc
import trasrv_pb2
import trasrv_pb2_grpc
import time 
from constant import *
import tra_test


def test_local_client():
    #channel = grpc.insecure_channel('172.16.103.142:50051')
    channel = grpc.insecure_channel('localhost:50051')
    stub = trasrv_pb2_grpc.traServerStub(channel)
    sync_request = trasrv_pb2.syncRequest(replica_id=tra_test.clientID)
    request = sync_request.vectors.add()
    # Client must update its sync vector to reflect the current sync    
    syncVector = sync_request.syncVector
    syncVector.replica = tra_test.clientID # get Client's UUID. Use hardcoded value for now
    # Use current time t-1 for sync request 
    # Server will store the result of the sync at t-1 using the current time t  
    syncVector.timestamp = 1 #int(time.time())-1 
    #request_file.file = 'root/test.txt'
    
    ##### TESTING ONLY - START ######
    # Test Case 1: File sync COPY
    #sync_request.type = 1 # syncRequestType.REQUEST_FILE
    #request.file = 'dir1/blah.txt' 
    #sync_request.type = 1
    #modVector1 = request.modVector.add()
    #modVector1.replica = 2
    #modVector1.timestamp = 1
    #modVector2 = request.modVector.add()
    #modVector2.replica = 2
    #modVector2.timestamp = 3
    #syncVector1 = request.syncVector.add()
    #syncVector1.replica = 2
    #syncVector1.timestamp = int(time.time())-1 
    
    # Test Case 1.1: File sync DO NOTHING Figure 2 (a)
    #request.file = 'blah.txt' 
    #sync_request.type = 1
    #modVector1 = request.modVector.add()
    #modVector1.replica = 1
    #modVector1.timestamp = 1
    #syncVector1 = request.syncVector.add()
    #syncVector1.replica = 1
    #syncVector1.timestamp = 1  
    #syncVector2 = request.syncVector.add()
    #syncVector2.replica = 2
    #syncVector2.timestamp = 2  
    
    # Test Case 1.2: File sync B -> A = COPY Figure 2 (b)
    request.file = 'blah.txt' 
    sync_request.type = 1
    modVector1 = request.modVector.add()
    modVector1.replica = tra_test.serverID
    modVector1.timestamp = 1
    modVector2 = request.modVector.add()
    modVector2.replica = tra_test.clientID
    modVector2.timestamp = 3 #int(time.time())-1
    syncVector1 = request.syncVector.add()
    syncVector1.replica = tra_test.serverID
    syncVector1.timestamp = 1  
    syncVector2 = request.syncVector.add()
    syncVector2.replica = tra_test.clientID
    syncVector2.timestamp = 2  
    syncVector3 = request.syncVector.add()
    syncVector3.replica = tra_test.clientID
    syncVector3.timestamp = 3 #int(time.time())-1
    
    
    # Test Case 1.3: File sync B -> A = REPORT CONFLICT Figure 2 (c)
    #request.file = 'dir1/blah.txt' 
    #sync_request.type = 1
    #modVector1 = request.modVector.add()
    #modVector1.replica = 1
    #modVector1.timestamp = 1
    #modVector2 = request.modVector.add()
    #modVector2.replica = 2
    #modVector2.timestamp = 3 #int(time.time())-1
    #syncVector1 = request.syncVector.add()
    #syncVector1.replica = 1
    #syncVector1.timestamp = 1  
    #syncVector2 = request.syncVector.add()
    #syncVector2.replica = 2
    #syncVector2.timestamp = 2  
    #syncVector3 = request.syncVector.add()
    #syncVector3.replica = 2
    #syncVector3.timestamp = 3 #int(time.time())-1
    
    # Test Case 2: Dir sync
    #sync_request.type = 0 # syncRequestType.REQUEST_DIR
    #sync_request.dir = 'dir1/' # Note '/' is needed to identify directory
    #modVector1 = sync_request.modVector
    #modVector1.replica = 2
    #modVector1.timestamp = 3

    # Test Case 3.1: File deletion (Do nothing)
    # "blah.txt" does not exist on server A. Sync(B -> A) should do nothing.
    #     1   2   3   4
    # A   X   X   -  ~>-
    # B     ~>X   X      
    #request.file = 'blah.txt' 
    #sync_request.type = 1
    #modVector1 = request.modVector.add()
    #modVector1.replica = tra_test.serverID
    #modVector1.timestamp = 1
    #syncVector1 = request.syncVector.add()
    #syncVector1.replica = tra_test.serverID
    #syncVector1.timestamp = 1  
    #syncVector2 = request.syncVector.add()
    #syncVector2.replica = tra_test.clientID
    #syncVector2.timestamp = 2   
    #creationVector = request.creationVector
    #creationVector.replica = tra_test.serverID
    #creationVector.timestamp = 1
       
    ##### TESTING ONLY - END ######
    
    response = stub.sync(sync_request)
    print "tra test client received ack = {} from replica {}: ".format(response.result, response.replica_id)

    # Test Case 1.2 - Copy file over.
    for result_index in range(len(response.result)):
        if response.result[result_index] == syncResult.COPY:
            file_path = sync_request.vectors[result_index].file
            file_request = trasrv_pb2.fileRequest(path=file_path)
            responseGen = stub.download(file_request)
            response = responseGen.next()
            status = response.status
            bytes = response.message
            print(status)
            
            outfile = None
            if (status > 0):
                outfile = open(file_path, 'wb')
            
            while (status > 0):
                outfile.write(bytes)
                response = responseGen.next()
                status = response.status
                bytes = response.message

            if outfile != None:
                outfile.close()

if __name__ == '__main__':
    test_local_client() 
