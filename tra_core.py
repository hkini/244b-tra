import os
import time
import constant
from constant import *
import tra_test
from util import setVector, updateVectorForReplica, print_vector, max_vector, traVectorPair, create_traVectorPair

#import tra_fs

'''
This file contains the tra core algorithm for file syncing.
Each traCore acts as a node for each directory.
'''

from enum import Enum
import os.path


class traCore:
    def __init__(self, path, replicaID=None, absolute_full_path=None):
        self.path = path
        self.absolute_full_path = absolute_full_path
        self.version = 1
        self.replicaID = replicaID
        self.parent = None
        #self.modVector = {} # Each node whether dir or file has a mod and sync vector
        #self.syncVector = {}
        self.child = {} # Store children traCores, keyed by path
        self.files = {} # store vector time pairs, keyed by filename
        self.vector_pair = traVectorPair() 

    def set_parent(self, p):
        self.parent = p
        self.replicaID = p.replicaID

    def add_child(self, c):
        if c != None:
            self.child[os.path.basename(c.path)] = c

    def get_child(self, path):
        if path in self.child:
            return self.child[path]
        else:
            return self.create_child(path)

    def create_child(self, path):
        new_child = traCore(path, self.replicaID)
        new_child.parent = self
        self.child[path] = new_child
        return new_child

    def add_file(self, f, vector_pair=None):
        if f != None:
            if vector_pair == None:
                vector_pair = traVectorPair()
            self.files[f] = vector_pair

    # Tra fs will handle the mtime percolating up the tree
    def mtimeup(self, file, currCore, childMod): 
        return None   

    def mtimeup_dir(self, currCore, childMod): 
        if currCore:
            # Percolate mtimeup the tree
            # m_B(path) = max(m_B(path), m)
            currCore.vector_pair.modVector = max_vector(currCore.vector_pair.modVector, childMod)
            # if path has parent
            #	mtimeup(parent(path))
            self.mtimeup_dir(currCore.parent, currCore.vector_pair.modVector)
        else:
            # no more parent up the tree to percolate mtime
            pass
        return None
        
    def percolate_up(self, curr_core, vector): 
        if curr_core:
            curr_core.vector_pair.syncVector = max_vector(curr_core.vector_pair.syncVector, vector)
            self.percolate_up(curr_core.parent, curr_core.vector_pair.syncVector)
        else:
            # no more parent up the tree to percolate sync vectors up
            pass
        return None
        
    def resolve_conflict(self, file, replica, ts):
        if constant.VERBOSE: print "Conflicted on file {} is resovled by chosing {}, {} version".format(file, replica, ts)
        # return 0 if fail to resolve
        # return 1 if resolved
        if constant.VERBOSE: print "Original mod replica " + str(replica) 
        if replica == -1: # Take server's version
            if not file in self.files: # File must have been deleted. Store max for directory
                resolvedReplica = self.vector_pair
                # Incorporate both replica's mod to reflect directory has changed since conflict 
                resolvedReplica.modVector = max_vector(resolvedReplica.modVector, resolvedReplica.conflictedModVector)
            else:
                resolvedReplica = self.files[file]
                # Need to use mod vector BEFORE the conflict
        else: 
            if not file in self.files: # File must have been deleted. Store max for directory
                resolvedReplica = self.vector_pair
                # Reset modification history to chosen replica's
                resolvedReplica.modVector = max_vector(resolvedReplica.modVector, resolvedReplica.conflictedModVector)
            else:
                resolvedReplica = self.files[file]
                # Reset modification history to chosen replica's
                resolvedReplica.modVector = resolvedReplica.conflictedModVector
        resolvedReplica.conflictedModVector = {} 
        if constant.VERBOSE: print "new mod replica " 
        if constant.VERBOSE: print  resolvedReplica.modVector
        if constant.VERBOSE: print "Original sync vector"
        if constant.VERBOSE: print resolvedReplica.syncVector
        if constant.VERBOSE: print "Conflicted vector"
        if constant.VERBOSE: print resolvedReplica.conflictedSyncVector
        resolvedReplica.syncVector = max_vector(resolvedReplica.syncVector, resolvedReplica.conflictedSyncVector)
        resolvedReplica.conflictedSyncVector = {} 
        resolvedReplica.conflictResolved = 1
        if constant.VERBOSE: print "Union of two"
        if constant.VERBOSE: print resolvedReplica.syncVector        
        return 1

    def copy(self, replica, file, remote_vector):
        # A copy must update the local modVector to include the sync at t-1 from remote replica and new version at t
        # sync at t-1 from remote replica
        # NOTE: There is a single latest entry for each replica in syncVector per format imposed in create_vector
        # m_B = m_A
        # If this is a new file, add it to the children files list
        if not file in self.files.keys():
            # Add to files
            self.add_file(file)
        # mB = mA 
        self.files[file].modVector = remote_vector.modVector
        # sB = max(sA, sB)
        self.files[file].syncVector = max_vector(self.files[file].syncVector, remote_vector.syncVector)
        # cB = cA (Copy the creation vector too)
        self.files[file].creationVector = remote_vector.creationVector
        # Update sync vector to contain current timestamp for current replica
        # if it doesn't exist, create new vector and add to sync 
        updateVectorForReplica(self.replicaID, self.files[file].syncVector)
        if constant.VERBOSE: print "Vectors for new file " + str(file)
        if constant.VERBOSE: print_vector(self.files[file])
        
        # Percolate modification time up the tree
        self.mtimeup(file, self.parent, self.files[file].modVector)
        if constant.VERBOSE: print "Copy"
        #self.files[file].conflictedTimestamp = remote_vector.modVector
        return syncResult.COPY

    def copy_dir(self, replica, dir, remote_vector):
        # A copy must update the local modVector to include the sync at t-1 from remote replica and new version at t
        # sync at t-1 from remote replica
        # NOTE: There is a single latest entry for each replica in syncVector per format imposed in create_vector
        # m_B = m_A
        # mB = mA 
        self.vector_pair.modVector = remote_vector.modVector
        # fs will do this
        # sB = max(sA, sB)
        # Need to do this after all children has been synced.
        # Ow children will be using new sync vector of its parent, which is not the state at t-1
        # cB = cA (Copy the creation vector too)
        self.vector_pair.creationVector = remote_vector.creationVector
        if constant.VERBOSE: print "Vectors for new dir " + str(dir)
        if constant.VERBOSE: print_vector(self.vector_pair)
        
        if constant.VERBOSE: print "Copy"
        return syncResult.COPY        
    def delete_file(self, file, remote_vector):
        joined_path = os.path.join(self.absolute_full_path, file)  
        if constant.VERBOSE: print "Deleting file " + file  + " path " + joined_path        
        try:
            os.remove(joined_path)
        except OSError:
            pass            
        # fs should already handle this!    	
        # Delete all metadata from the file
        #del self.files[file]
        # Create deletion notice by updating directory's sync vector
        #self.vector_pair.syncVector = remote_vector.syncVector
        return syncResult.DELETE                        

    # When conflicts are resolved, modification time is set to chosen resolution.
    # sync time set to union of max of two sync times on conflicting files.
    # So resolved file knows about both files conflicted.
    def report_conflict(self, replica, file, remote_vector):
        if constant.VERBOSE: print "Detected a conflict for " + file
        if constant.VERBOSE: print_vector(remote_vector)
        if file in self.files.keys():
            # Save off vectors to save conflict resolution state later
            conflictedThing = self.files[file]
        else: # File must have been deleted, but we still have to report the CONFLICT
            conflictedThing = self.vector_pair # Store in parent
        conflictedThing.conflictedSyncVector = remote_vector.syncVector
        conflictedThing.conflictedModVector = remote_vector.modVector
        return syncResult.CONFLICT

    # sync(A->B)
    # f_B does not exist on B
    # Check if each vector is <= to each corresponding vector on another replica.
    # Result is a bit vector determining if ALL vectors are <= its corresponding vector.
    # Result bit 0 is true if a vector is <= to its corresponding vector.
    # Result bit 1 is true if a vector is > to its corresponding vector.
    # Result = 1 if for all vectors, each vector is <= to its corresponding vector.
    # Result = 2 if for all vectors, each vector is > its corresponding vector.
    # Result = 3 if only some vectors are <= to its corresponding vectors
    # and others are > than its corresponding vectors.
    def sync_deletion(self, replica, A, B):
        if constant.VERBOSE: print "Sync deletion or new file, A = {}".format(A.creationVector)
        
        #'''
        if constant.VERBOSE: print "A's vectors"
        if constant.VERBOSE: print_vector(A)
        if constant.VERBOSE: print "B's vectors"
        if constant.VERBOSE: print_vector(B)        
        #'''

                
        result = 0 	# 1 := remote A <= local B
                    # 2 := independent files
                    # o.w. := conflict
                    
        # We must know the creation time of the remote copy.
        # "The creation time is the first element in the file's modification history."

        for m in A.creationVector.keys(): # Only expect 1 entry
            if constant.VERBOSE: print "Creation vector: {" + str(m) + " : " + str(A.creationVector[m]) + " } "
            # The replica m creating the file is not found in local sync vectors
            if not m in B.syncVector.keys():
                result = result | 2 # File must be independently created in remote replica
                break # No need to keep checking: local does not have entries for remote replica creating the file
            else:
                # if c_A <= s_B
                # Local replica s_B knows about remote replica m creating the file c_A.
                if A.creationVector[m] <= B.syncVector[m]:
                    result = result | 1 # local B has a deletion notice for remote A's file

                    # if m_A <= s_B
                    for m in A.modVector.keys():
                        if not m in B.syncVector.keys(): # m_A has entries not found in local sync vector s_B
                            result = result | 2 # m_a contains entries not in s_b
                            break # No need to keep checking bc m_A cannot be a prefix of s_B
                        else:
                            if A.modVector[m] <= B.syncVector[m]:
                                result = result | 1
                            else:
                                result = result | 2
                                break # No need to keep checking: local entry timestamp is newer than remote

                else: # B has never heard of this file
                    result = result | 2
                    break # No need to keep checking: local entry timestamp is newer than remote

        if result == 1:
            if constant.VERBOSE: print "sync_deletion: (m_A <= s_B) " + str(A.modVector) + " <= " + str(B.syncVector)
        elif result == 2:
            if constant.VERBOSE: print "sync_deletion: (independent) " + str(A.modVector) + " > " + str(B.syncVector)

        return result

    def compare_tra_vectors(self, replica, A, B):
        # Figure 9: Single-file synchronization using vector time pairs
        # if m_a <= s_b
        # 	do nothing
        # else if m_b <= s_a
        #	copy F_a to b
        # else
        #	report conflict
        #'''
        if constant.VERBOSE: print "A's vectors"
        if constant.VERBOSE: print_vector(A)
        if constant.VERBOSE: print "B's vectors"
        if constant.VERBOSE: print_vector(B)        
        #'''

        result = 1 	# 1 := local <= remote (Assume)
                    # 2 := local >= remote
                    # o.w. := local != remote
        # Case 1: local <= remote
        for m in A.modVector.keys():
            if not B or not m in B.syncVector.keys(): # empty set [] <= any set
                result = result | 2 # m_a contains entries not in s_b
                break # No need to keep checking: local has entries not found in remote
            else:
                if A.modVector[m] <= B.syncVector[m]:
                    result = result | 1
                else:
                    result = result | 2
                    break # No need to keep checking: local entry timestamp is newer than remote

        if result == 1:
            # do nothing
            if constant.VERBOSE: print "compare_tra_vectors (A->B): (mA) " + str(A.modVector) + " <= (sB) " + str(B.syncVector)
            return result

        result = 2 # Assume local >= remote
        # Case 2: local >= remote
        if not B:
            return 1 # Always copy if we have no history of it
        for m in B.modVector.keys():
            if not A or not m in A.syncVector.keys():
                result = result | 1; # No need to keep looking: remote has entries not in local
                break
            else:
                if A.syncVector[m] >= B.modVector[m]:
                    result = result | 2
                else:
                    result = result | 1 # No need to keep checking: local entry's timestamp is older than remote
                    break
        if result == 2:
                if constant.VERBOSE: print "compare_tra_vectors (A->B): (mB) " + str(B.modVector) + " <= (sA) " + str(A.syncVector)
        if constant.VERBOSE: print "compare_tra_vectors (A->B): (mB) " + str(B.modVector) + " ? (sA) " + str(A.syncVector)
        return result

    # Client has a file. Server does not.
    # sync(A->B)
    def found_file(self, replica, file, modVector, syncVector, creationVector):
        if constant.VERBOSE: print "TRA_CORE: Found file {}".format(file)
        # This file may have been deleted from local. Check parent's sync time.
        # Construct sync vector from parent's sync time.
        remote_vector = create_traVectorPair(modVector, syncVector, creationVector)
        # Test Cases 3.x: File deletion
        # The only metadata needed for deleted file is the parent's synchronization time
        # For testing purposes only

        if self.parent:
            # If there is a parent, check that file hasn't already been deleted.

            result = self.sync_deletion(replica, remote_vector, self.parent.vector_pair)
            if result == 1: # m_A <= s_B
                if constant.VERBOSE: print "Do nothing on local. Server has already deleted the Client's file."
                return syncResult.DO_NOTHING
            elif result == 2: # c_A not <= s_B
                # Independent file creation
                if constant.VERBOSE: print "Copy"
                # Update local modification time and tell Client to push the file over
                return self.copy(replica, file, remote_vector)
            else:
                # According to the paper Figure 4 c, sync should report conflict
                # because "neither version is derived from the other"
                return self.report_conflict(replica, file, remote_vector)
        else:
            # If there is no parent => no history of this file. Must be a new file.
            # Copy over.
            if constant.VERBOSE: print "No parent directory. Since there is no history on this file, assume new file. COPY"
            return self.copy(replica, file, remote_vector)

    def found_dir(self, replica, modVector, syncVector, creationVector):
        if constant.VERBOSE: print "TRA_CORE: Found dir {}!".format(self.path)
        remote_vector = create_traVectorPair(modVector, syncVector, creationVector)
        # Don't you just do the same thing as for file?

        if self.parent:
            # If there is a parent, check that file hasn't already been deleted.
            result = self.sync_deletion(replica, remote_vector, self.parent.vector_pair)
            if result == 1: # m_A <= s_B
                if constant.VERBOSE: print "Do nothing on local. Server has already deleted the Client's file."
                return syncResult.DO_NOTHING
            elif result == 2: # c_A not <= s_B
                # Independent file creation
                if constant.VERBOSE: print "FOUND_DIR : Copy " + self.path
                # Update local modification time
                return self.copy_dir(replica, self.path, remote_vector)
            else:
                # For a file that does not exist, we should not ever report conflict.
                print "FOUND_DIR : NOT EXPECTED!"
                return syncResult.DO_NOTHING
        else:
            # If there is no parent => no history of this dir. Must be a new file.
            # Copy over.
            return self.copy_dir(replica, self.path, remote_vector)

    # sync(B->A)
    # This function is for directory syncing when we have files that do not exist on
    # the client. If the client does not have the dir and the server does, it means
    # either:
    #    1) The client S1 has already deleted the file => server S2 DELETE
    #    2) The file was created independently. Client never had the file to begin
    #		with => DO NOTHING The server and client should do nothing since the sync
    # is unidirectional. Ie., if there is data exchange, it should only be from client
    # to server.
    # Missing file needs remote's vectors to check if this local file has been deleted 
    # on remote  
    def missing_file(self, replica, file, parent_sync_vector):#,  modVector, syncVector, creationVector):
        if constant.VERBOSE: print "TRA_CORE: Missing file {}, dir {}".format(file, self.path)
        remote_vector = create_traVectorPair({}, parent_sync_vector, {})

        # sync(B->A)
        # A does not have have the file.
        result = self.sync_deletion(replica, self.files[file], remote_vector)
        if result == 1: # m_B <= s_A
            if constant.VERBOSE: print "Client has already deleted the Server's file. Server will DELETE"
            return self.delete_file(file, remote_vector) # Let Client know Server is deleting its file?
        elif result == 2: # c_B not <= s_A
            # Independent file creation
            if constant.VERBOSE: print "Do nothing"
            return syncResult.DO_NOTHING
        else:
            # For a file that does not exist, we should not ever report conflict.
            if constant.VERBOSE: print "File vector is neither prefix or independent? This is a conflict."
            return syncResult.CONFLICT

    def missing_dir(self, replica):
        if constant.VERBOSE: print "TRA_CORE: Missing directory {}".format(self.path)
        # Use the server parent's vectors as the client's parent
        # vectors because after dir sync, which happens before file_sync/missing_file
        # is invoked, the two parent vectors should be the same.

        # sync(B->A)
        # A does not have have the file.
        result = self.sync_deletion(replica, self.vector_pair, self.parent.vector_pair)
        if result == 1: # m_B <= s_A
            if constant.VERBOSE: print "Client has already deleted the Server's file. Server will DELETE"
            #self.delete(file)

            return self.delete_file(self.path, self.parent.vector_pair) # Let Client know Server is deleting its file?
        elif result == 2: # c_B not <= s_A
            # Independent file creation
            if constant.VERBOSE: print "Do nothing"
            return syncResult.DO_NOTHING
        else:
            # For a file that does not exist, we should not ever report conflict.
            if constant.VERBOSE: print "Dir is neither prefix or indepenent. DO NOTHING by default!"
            return syncResult.DO_NOTHING

    def sync_file(self, replica, file, modVector, syncVector, creationVector):
        if constant.VERBOSE: print "SYNC_FILE: syncing {}".format(file)
        if not file:
            # should not reach here
            return syncResult.FAULT
        if file not in self.files:
            raise "bug, file not in tracore"
        # Convert remote mod and sync Vectors to local traVectorPair format
        remote_vector = create_traVectorPair(modVector, syncVector, creationVector)

        # Case 2: 	File found
        #			Compare version and sync vectors

        # Figure 9: Single-file synchronization using vector time pairs
        if constant.VERBOSE: print "A client " + file + " B server " 
        result = self.compare_tra_vectors(replica, remote_vector, self.files[file])
        if result == 1 : # mA <= sB, where A = remote vector, B = local vector
                # F_B Remote (client) is the same as or derived from F_A Local (server)
                # do nothing
            if constant.VERBOSE: print "Do nothing"
            # Update sync vector to contain all the info
            # sB = max(sA, sB)
            self.files[file].syncVector = max_vector(remote_vector.syncVector, self.files[file].syncVector)
            return syncResult.DO_NOTHING
        elif result == 2: # remote <= local
                # F_A local is derived from F_B remote
                # self.copy(replica, file, remote_vector)
                # use rpc reply to resolve copying
            return self.copy(replica, file, remote_vector)
        else:
                # Neither file is derived from the other.
            return self.report_conflict(replica, file, remote_vector)
            # should not reach here
        return syncResult.FAULT


    # Figure 11: Directory synchronization using vector time pairs
    # The sync is considering a single directory that exists on A and B.
    # sync(A -> B, dir)
    # This looks the same as sync_file. We may be able to combine depending
    # on how we implement the recursive child sync.
    def sync_dir(self, replica, dir, modVector, syncVector, creationVector):
        if constant.VERBOSE: print " SYNC_DIR: start syncing dir {} ".format(dir)
        if not dir:
            # should not reach here
            return syncResult.FAULT

            # Case 2: 	File found
            #			Compare version and sync vectors

            # Convert remote mod and sync Vectors to local traVectorPair format
        remote_vector = create_traVectorPair(modVector, syncVector, creationVector)

        # Must be start because directory modVector is empty. Neither created by server or 
        # synced by client. Ow directory modVector can never be empty bc it's always the 
        # union of modVectors of its children. 
        if not self.vector_pair.modVector:
            return self.copy_dir(replica, dir, remote_vector)

        # Figure 9: Single-file synchronization using vector time pairs
        if constant.VERBOSE: print "B server " + self.path + " ? A client " + str(dir)
        result = self.compare_tra_vectors(replica, remote_vector, self.vector_pair)
        if result == 1: # B <= A
                # Local already has all the changes present in remote's tree
                # do nothing
            if constant.VERBOSE: print "SYNC_DIR: DO nothing dir {}".format(self.path)
            ret = syncResult.DO_NOTHING
        else:
            # Recurse into tree
            if constant.VERBOSE: print "SYNC_DIR: COPY dir {}".format(self.path)
            return self.copy_dir(replica, dir, remote_vector)
            # should not reach here
        return syncResult.FAULT


########### Other helper functions #############

'''
Return the tra_core associate with the given path
'''
def find_core(parent, path):
    if not parent:
        print "Bad argument: cannot to look for " + str(path) + " within " + str(parent)
        return None
    if os.path.commonprefix([path, parent.path]) != parent.path:
        # check if this tra core contains the queried path
        # doesn't match in this case
        return None
    else:
        if parent.path == path:
            # reach the directory, return itself
            return parent
        else:
            # recursively search in children nodes
            for c in parent.child:
                result = find_core(parent.child[c], path)
                if result:
                    # found the directory!
                    return result
            # didn't find anything
            return None
# find the parent tra core for a given file
def find_file(parent, file):
    if constant.VERBOSE: print parent, file
    return find_core(parent, os.path.dirname(file))
