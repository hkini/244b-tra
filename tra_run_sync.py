from tra_client import stubRunner, syncClientJobFile, syncClientJobDir
import grpc
import tra_fs
import sys, os.path
import util
import argparse
import constant

'''
This is the run time script to run a sync request for a given file or directory
'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("LOCAL_DIR", type=str, help="the local directory to sync over")
    parser.add_argument("REMOTE_DIR", type=str, help="the remote directory to sync to")
    parser.add_argument("--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("--uuid", help="set uuid. useful for testing", type=int)
    parser.add_argument("--port", help="set port to connect to", type=int, default=50051)
    parser.add_argument("--host", help="set ip of host", default='127.0.0.1')
    args = parser.parse_args()
    
    if args.uuid != None:
        util.set_uuid(args.uuid)
    
    ip = args.host + ':' + str(args.port)
    
    constant.VERBOSE = args.verbose
    print(constant.VERBOSE)
    channel = grpc.insecure_channel(ip)
    stub_runner = stubRunner(channel)
    test_client_path = args.LOCAL_DIR
    test_remote_path = args.REMOTE_DIR
    _, core = tra_fs.populate_tra_cores(path=test_client_path)

    if not os.path.exists(test_client_path):
        print "Error: client path {} does not exist!".format(test_client_path)

    if os.path.isfile(test_client_path):
        test_client_dir, file = os.path.split(test_client_path)
        test_remote_dir = os.path.split(test_remote_path)[0]
        tra_fs.update_tra_core_files_no_recurse(core, path=test_client_dir)
        job = syncClientJobFile(core, stub_runner, test_remote_dir, file)
    else:
        test_client_dir = test_client_path
        test_remote_dir = test_remote_path
        core = tra_fs.update_tracore_recursively(core, path=test_client_dir)
        job = syncClientJobDir(core, stub_runner, test_remote_dir)

    job.run()
    tra_fs.dump_tra_cores(core, test_client_dir, recursively_dump=os.path.isdir(test_client_path))

