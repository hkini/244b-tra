from enum import IntEnum

'''
This file contains constant/enum definition that are shared among 
all tra files.
'''

class syncRequestType(IntEnum):
    REQUEST_DIR = 0
    REQUEST_FILE = 1
    REQUEST_COMPLETE = 2

class syncResult(IntEnum):
    DO_NOTHING = 0
    CONFLICT = 1
    COPY = 2
    DELETE = 3
    COMPLETE = 4
    FAULT = 5

class syncState(IntEnum):
    SYNCING_FILE = 0
    SYNCING_DIR = 1
    SYNCING_FAULT = 2

RSYNC_BLOCK_SIZE = 256
RSYNC_THRESHOLD = 4096

UUID_FILE = '.tra_uuid'

UUID_PATH = '~/.tra_uuid'

VERBOSE = False

