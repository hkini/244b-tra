import tra_fs, tra_core, sys
import os.path
import time
import util

def print_usage():
    print "usage: [selected conflict resolution filename]"


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print_usage()
        exit()
    resolved_file_full = sys.argv[1]
    base_dir ,resolved_file = os.path.split(sys.argv[1])
    if not base_dir:
        base_dir = "."
    if not os.path.exists(base_dir) or not os.path.isfile(resolved_file_full):
        print_usage()
        exit()

    last_index = resolved_file.find(tra_fs.TRACORE_CONFLICT_SUFFIX)
    if last_index != -1:
        og_filename = resolved_file[:last_index]
        id, ts = resolved_file[last_index + len(tra_fs.TRACORE_CONFLICT_SUFFIX) + 1:].split("_")
    else:
        # double check if there is other atually conflicted versons
        og_filename = resolved_file
        count = 0
        for file in os.listdir(base_dir):
            if og_filename in file:
                count += 1
            if count >=2:
                break
        if count < 2:
            print "Error No other conflicted version for file {}".format(og_filename)
            exit()
        id = -1
        ts = -1

    print "Resolving conflict for {} using UUID version {}".format(og_filename, id)

    # open up tracore
    _, core = tra_fs.populate_tra_cores(os.path.join(os.getcwd(), resolved_file_full))
    if not core:
        print "Error: fail to load tra core."
        exit()
    if core.resolve_conflict(og_filename, int(id), int(ts)):
        # delete the rest of file and make resolved_file og_File
        os.rename(resolved_file_full, os.path.join(base_dir, og_filename))
        for file in os.listdir(base_dir):
            print base_dir
            print file
            if file != og_filename and og_filename == file[:file.find(tra_fs.TRACORE_CONFLICT_SUFFIX)]:
                os.remove(os.path.join(base_dir,file))
        t = int(time.time())  
        if og_filename in core.files:
            core.files[og_filename].conflictedTimestamp = t
        else: # File must have been deleted already. Store in parent
            core.vector_pair.conflictedTimestamp = t      
        print "Conflicted timestamp " + str(t) 
    tra_fs.dump_tra_cores(core, core.absolute_full_path)





