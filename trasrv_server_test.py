from trasrv_server import traServer
import sys
import tra_test
import constant
import argparse

'''
This is a simple tra server runtime that will aceept syncRequests from clients.
ack
'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("--uuid", help="set uuid. useful for testing", type=int)
    parser.add_argument("--port", help="set port to connect to", type=int, default=50051)
    args = parser.parse_args()
    constant.VERBOSE = args.verbose
    
    print "starting tra server"
    s = traServer(uuid=args.uuid, port=args.port)
    s.run()
