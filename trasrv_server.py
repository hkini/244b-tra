import grpc
import time
import trasrv_pb2
import trasrv_pb2_grpc
import tra_fs
from concurrent import futures
from tra_core import traCore
import constant
from constant import *
import tra_core
import os.path
import util
import zlib, hashlib
from util import setVector, updateVectorForReplica, print_vector, max_vector, traVectorPair, create_traVectorPair


'''
This file contains network and rpc server class
'''


#TODO how to bootstrap traNetwork?

#TODO assuming file names are unique across all directories now. need to change this



class traNetwork:
    def __init__(self):
        self.replicas = {}
    def add_replica(self, hostname, port):
        # TODO place replica network info here
        # assume each replica is keyed by an ID
        pass

# TODO Each syncServerJob can be run in parallel, but syncServerJob has to lock the core if it scanning any of it's children
# TODO if we use DFS, it requires less locking

class syncServerJob:
    def __init__(self):
        # constructor start a sync job at root path
        self.replica = -1
        self.current_parent = None
        self.current_core = None
        self.state = syncState.SYNCING_FAULT
        self.outstanding_child = []  # outstanding objects to sync up, use as a queue
        self.outstanding_files = []
        self.job_id = -1
        self.root_core = None
        self.parent_sync_vector = {}
        self.parent_lists = {}
        self.new_parent_lists = {}
        self.outstanding_copies = {}


    def start(self, job_id, request):
        if constant.VERBOSE: print("Starting off a server job to sync up " + request.dir)

        abs_expand_path = os.path.abspath(os.path.expanduser(request.dir))
        _, self.current_core = tra_fs.populate_tra_cores(request.dir)
        if not self.current_core:
            # check if we are syncing a new directory/file
            if constant.VERBOSE: print os.path.split(abs_expand_path)[0]
            if os.path.exists(os.path.split(abs_expand_path)[0]):
                self.current_core = tra_fs.create_new_dir_no_parent(abs_expand_path)
            if not self.current_core:
                self.state = syncState.SYNCING_FAULT
                print "TRA_SERVER: ERROR: request directory {} not found".format(request.dir)
                return [syncResult.FAULT]
        # now start syncing
        self.root_core = self.current_core
        if request.type == syncRequestType.REQUEST_FILE:
            if constant.VERBOSE: print "TRA_SERVER: Got sync file request {}, {}".format(request.dir, request.vectors[0].file)
            self.state = syncState.SYNCING_FILE
            tra_fs.update_tra_core_files_no_recurse(self.current_core, path=request.dir)
            if request.vectors[0].file not in self.current_core.files:
                sync_result = [self.current_core.found_file(request.replica_id, request.vectors[0].file,
                                                            request.vectors[0].modVector,
                                                            request.vectors[0].syncVector,
                                                            request.vectors[0].creationVector)]
            else:
                sync_result = [self.current_core.sync_file(request.replica_id, request.vectors[0].file,
                                        request.vectors[0].modVector,
                                        request.vectors[0].syncVector,
                                        request.vectors[0].creationVector)]
            self.add_to_outstanding_copies(sync_result)
        else:
            if constant.VERBOSE: print "TRA_SERVER: Start with sync dir request {}".format(request.dir)
            self.current_core = tra_fs.update_tracore_recursively(self.current_core, path=request.dir)
            self.state = syncState.SYNCING_DIR
            self.current_parent = self.current_core
            self.new_parent_lists[self.current_core.absolute_full_path] = self.current_core
            sync_result = [self.current_core.sync_dir(request.replica_id, request.dir,
                                                  request.modVector,
                                                  request.syncVector,
                                                    request.creationVector)]
            if sync_result[0] == syncResult.COPY.value:
                self.outstanding_child = self.current_core.child.keys()
                self.outstanding_files = self.current_core.files.keys()
            self.job_id = job_id
        return sync_result

    def progress(self, request):
        if constant.VERBOSE: print "progressing request for {}".format(request.dir)
        if constant.VERBOSE: print "current parent = {}, outstanding child = {}".format(self.current_parent.absolute_full_path, self.outstanding_child)
        if self.state == syncState.SYNCING_FAULT:
            return [syncResult.FAULT.value]

        if request.type == syncRequestType.REQUEST_COMPLETE.value:
            return [syncResult.COMPLETE.value]

        elif request.type == syncRequestType.REQUEST_DIR.value:
            if self.state == syncState.SYNCING_FILE:
                # done with syncing all files, clean up all outstandings
                for f in self.outstanding_files:
                    self.current_core.missing_file(self.replica, f, self.parent_sync_vector)

            self.state = syncState.SYNCING_DIR
            abs_request_dir = os.path.expanduser(request.dir)
            if self.current_parent.absolute_full_path == os.path.split(abs_request_dir)[0]:
                #if constant.VERBOSE: print "DEBUG: tra found {} under current parent {}".format(abs_request_dir, self.current_parent.absolute_full_path)
                if os.path.basename(request.dir) in self.current_parent.child:

                    self.outstanding_child.remove(os.path.basename(request.dir))
                    self.current_core = self.current_parent.child[os.path.basename(request.dir)]
                    self.new_parent_lists[self.current_core.absolute_full_path] = self.current_core
                    sync_result = [self.current_core.sync_dir(self.replica, request.dir,
                                                              request.modVector,
                                                              request.syncVector,
                                                              request.creationVector)]	

                else:
                    # create a new tra_core
                    self.current_core = tra_fs.create_new_dir_tracore(self.current_parent,
                                                                      os.path.basename(request.dir))
                    self.new_parent_lists[self.current_core.absolute_full_path] = self.current_core
                    sync_result = [self.current_core.found_dir(self.replica,
                                                                      request.modVector,
                                                                      request.syncVector,
                                                                      request.creationVector)]
                if sync_result[0] == syncResult.COPY.value:
                    self.outstanding_files = self.current_core.files.keys()
                self.parent_sync_vector = request.syncVector
            elif os.path.split(abs_request_dir)[0] in self.parent_lists:
                # found in other parents
                #if constant.VERBOSE: print "DEBUG: found {} under other parent in parent list {}".format(abs_request_dir, self.parent_lists.keys())
                new_parent_path = os.path.split(abs_request_dir)[0]
                for child in self.outstanding_child:
                    self.current_parent.child[child].missing_dir(self.replica)
                self.current_parent = self.parent_lists[new_parent_path]
                self.outstanding_child = self.current_parent.child.keys()
                self.parent_lists.pop(new_parent_path)
                sync_result = self.progress(request)
            else:
                # going to next level ? if the directory is child of any current child
                found = 0
                new_parent_path = os.path.split(abs_request_dir)[0]
                if new_parent_path in self.new_parent_lists:
                    # found in next level
                    #if constant.VERBOSE: print "DEBUG: found {} under next level parent in parent list {}".format(abs_request_dir, self.new_parent_lists.keys())
                    for child in self.outstanding_child:
                        self.current_parent.child[child].missing_dir(self.replica)
                    self.parent_lists = self.new_parent_lists
                    sync_result = self.progress(request)
                else:
                    print "WARNING - This is a BUGGGG, either u pass in the wrong pass or talk to Jennifer lol"
                    # should not reach here
                    self.state = syncState.SYNCING_FAULT
                    sync_result = [syncResult.FAULT.value]

            if self.outstanding_files:
                # expect to sync up files
                self.state = syncState.SYNCING_FILE
            return sync_result
        # syncing file
        else:
            sync_result = []
            for v in request.vectors:
                if v.file in self.outstanding_files:
                    sync_result.append(self.current_core.sync_file(self.replica, v.file, v.modVector, v.syncVector, v.creationVector))
                    self.outstanding_files.remove(v.file)
                else:
                    sync_result.append(self.current_core.found_file(self.replica, v.file, v.modVector, v.syncVector, v.creationVector))
            self.add_to_outstanding_copies(sync_result)
            return sync_result

    def add_to_outstanding_copies(self, sync_result):
        if syncResult.COPY.value in sync_result or syncResult.CONFLICT.value in sync_result:
            file_count = sync_result.count(syncResult.COPY.value) + sync_result.count(syncResult.CONFLICT.value)
            if self.current_core.absolute_full_path not in self.outstanding_copies:
                self.outstanding_copies[self.current_core.absolute_full_path] = (self.current_core, file_count)
            else:
                _, old_count = self.outstanding_copies[self.current_core.absolute_full_path]
                self.outstanding_copies[self.current_core.absolute_full_path] = (self.current_core, file_count + old_count)

    def overwrite_mtime(self, file_abs_path):
        dir_name, file = os.path.split(file_abs_path)
        core, count = self.outstanding_copies[dir_name]
        if dir_name not in self.outstanding_copies:
            return False
        if file in core.files:    
            core.files[file].conflictedTimestamp = int(os.path.getmtime(file_abs_path))
        count -= 1
        if count == 0:
            self.outstanding_copies.pop(dir_name)
        else:
            self.outstanding_copies[dir_name] = (core, count)
        return True


    def complete(self):
        for f in self.outstanding_files:
            self.current_core.missing_file(self.replica, f, self.parent_sync_vector)
        for child in self.outstanding_child:
            self.current_parent.child[child].missing_dir(self.replica)
        remote_vector = create_traVectorPair({}, self.parent_sync_vector, {})        
        # Update sync info and percolate it up
        self.current_core.percolate_up(self.current_core, remote_vector.syncVector)
        tra_fs.dump_tra_cores(self.root_core, self.root_core.absolute_full_path, recursively_dump=True)



class traService(trasrv_pb2_grpc.traServerServicer):
    def __init__(self):
        self.jobs = {}
        self.job_counter = 0
        self.rsync_jobs = {}
        self.rsync_counter = 1

    def overwrite_mtime(self, request, context):
        reply = trasrv_pb2.overwriteReply()
        job = self.jobs[request.id]
        if job.overwrite_mtime(os.path.expanduser(request.file_path)):
            reply.status = True
        else:
            reply.status = False
            raise "bug in overwritemtime server"
        return reply

    def sync(self, request, context):
        reply = trasrv_pb2.syncReply(replica_id=util.get_uuid())
        if request.type == syncRequestType.REQUEST_COMPLETE:
            self.jobs[request.job_id].complete()
            if constant.VERBOSE: print "TRA_SERVER: done with job {}".format(request.job_id)
            reply.results.append(syncResult.COMPLETE.value)
            return reply

        elif request.job_id == -1:
            # create new job
            if constant.VERBOSE: print "TRA_SERVER: got new request, assign to job {}".format(self.job_counter)
            _, root_core = tra_fs.populate_tra_cores(request.dir)
            job = syncServerJob()
            reply.job_id = self.job_counter
            sync_result = job.start(reply.job_id, request)
            self.jobs[reply.job_id] = job
            self.job_counter += 1
        else:
            # retrieve old job
            if constant.VERBOSE: print "TRA_SERVER: processing job {}, progress to {}".format(request.job_id, request.dir)
            job = self.jobs[request.job_id]
            sync_result = job.progress(request)
            reply.job_id = request.job_id
        for r in sync_result:
            reply.results.append(r)
        return reply

    def download(self, request, context):
        # TODO this should be encapsulated in tra_fs.py, where we have a lock on the file
        # so it doesn't get deleted between calling sync and then downloading. We should also
        # make sure it exists in the tra_core, for sanity's sake
        # TODO this request and response pair should do delta encoding
        for status, bytes in tra_fs.read_file_from_path(request.path):
            yield trasrv_pb2.fileChunkReply(status = status, message = bytes)

    def upload(self, fileChunkUpload, context):
        chunk = fileChunkUpload.next()
        with open(os.path.expanduser(chunk.path), 'wb') as of:
            while (chunk.status > 0):
                of.write(chunk.message)
                #if constant.VERBOSE: print("Writing file chunk with status: " + str(chunk.status))
                chunk=fileChunkUpload.next()
            of.flush()
            os.fsync(of)
        return trasrv_pb2.uploadReply(status = 0)

    def start_rsync(self, request, context):
        # return checksums at remote
        reply = trasrv_pb2.rsyncStartReply()
        user_path = os.path.expanduser(request.file_path)
        if constant.VERBOSE: print "got rsync request on file {}".format(user_path)
        if os.path.exists(user_path) and os.path.isfile(user_path):
            if constant.VERBOSE: print "starting rsync on {}".format(user_path)
            with open(user_path, 'r') as f:
                while True:
                    block_data = f.read(RSYNC_BLOCK_SIZE)
                    if not block_data:
                        break
                    checksum = reply.checksums.add()
                    checksum.week = zlib.adler32(block_data)
                    checksum.strong = hashlib.md5(block_data).digest()
            reply.id = self.rsync_counter
            self.rsync_jobs[reply.id] = user_path
            self.rsync_counter += 1
        else:
            print "invalid path {}".format(user_path)
            reply.id = -1
        return reply

    def rsync_upload(self, request, context):
        reply = trasrv_pb2.rsyncYieldReply()
        # assum file exist
        if constant.VERBOSE: print "Starting accepting rsync upload"
        delta = request.next()
        if os.path.exists(self.rsync_jobs[delta.id]):
            with open(self.rsync_jobs[delta.id] + "_out", 'w') as output_f:
                with open(self.rsync_jobs[delta.id],'r') as remote_f:
                    while delta.status > 0:
                        if delta.byte:
                            output_f.write(delta.byte)
                        else:
                            remote_f.seek(delta.offset)
                            #if constant.VERBOSE: print "got a matching block, copy data from remote"
                            output_f.write(remote_f.read(delta.length))
                        delta = request.next()
            os.rename(self.rsync_jobs[delta.id] + "_out", self.rsync_jobs[delta.id])
            reply.status = 1
        else:
            print "RSYNC: fail to accept an upload for {}".format(self.rsync_jobs[delta.id])
            reply.status = 0
        return reply


class traServer():
    def __init__(self, uuid=None, port=None, host=None):
        #self.root_path = path
        if uuid != None:
            util.set_uuid(uuid)
        self.port = port
        self.host = host

    def run(self):
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        servicer = traService()
        trasrv_pb2_grpc.add_traServerServicer_to_server(servicer, server)
        server.add_insecure_port('[::]:' + str(self.port))
        server.start()
        while True:
            time.sleep(1)


