import grpc
import trasrv_pb2
import trasrv_pb2_grpc



def test_local_client():
    channel = grpc.insecure_channel('localhost:50051')
    stub = trasrv_pb2_grpc.traServerStub(channel)
    sync_request = trasrv_pb2.syncRequest(replica_id=1)
    request_file = sync_request.requests.add()
    
    # Single-file synchronization using vector time pairs
    # Test Case: 1 
    #     1   2   3   4=now
    #  A    ~>X      ~>?
    #  B  X   X   Y
    # sync(B->A): COPY
    # F_B is newer than F_A: Copy F_A to B
    # Client: B 	
    #			modVector: {"B":2} # By optimization {"B":2} implies {"B":1, "B":2} 
    #			syncVector: {"B":3}  # Sync starting at B t=3
    # Server: A
    #			modVector: {"B":1}
    #			syncVector: {"B":1, "A":2}
    ###### CODE
    request_file.file = 'Dir1/blah.txt'
    modVector1 = request_file.modVector.add()
    modVector1.replica = "B"
    modVector1.timestamp = 1
    modVector2 = request_file.modVector.add()
    modVector2.replica = "B"
    modVector2.timestamp = 3
    syncVector1 = request_file.syncVector.add()
    syncVector1.replica = "B"
    syncVector1.timestamp = 3

    # Test Case: 2 
    #     1  2  3=now
    #  A      ~ ?
    #  B  X  Y
    # sync(A->B): COPY
    # Client: B 	
    #			modVector: {"B":2} # By optimization {"B":2} implies {"B":1, "B":2} 
    #			syncVector: {"B":3} 
    # Server: A
    #			modVector: {}
    #			syncVector: {}
    ###### CODE
    #request_file.file = 'Dir1/blah.txt'
    #modVector1 = request_file.modVector.add()
    #modVector1.replica = "B"
    #modVector1.timestamp = 1
    #modVector2 = request_file.modVector.add()
    #modVector2.replica = "B"
    #modVector2.timestamp = 2
    #syncVector1 = request_file.syncVector.add()
    #syncVector1.replica = "B"
    #syncVector1.timestamp = 3
    
	# Test Case: 3
	#     1  2  3=now
	#  A     ~X ~? 
	#  B  X   Y
	# sync(B->A): Do nothing again
	# Client: B 	
	#			modVector: {"B":2} # By optimization {"B":2} implies {"B":1, "B":2} 
	#			syncVector: {"B":3} 
	# Server: A
	#			modVector: {"B":1}
	#			syncVector: {"B":1, "A":2}	
    ###### CODE
    #request_file.file = 'Dir1/blah.txt'
    #modVector1 = request_file.modVector.add()
    #modVector1.replica = "B"
    #modVector1.timestamp = 1
    #modVector2 = request_file.modVector.add()
    #modVector2.replica = "B"
    #modVector2.timestamp = 2
    #syncVector1 = request_file.syncVector.add()
    #syncVector1.replica = "B"
    #syncVector1.timestamp = 3
    
    # Test Case: 4 File CREATE TODO 
    #     1  2  3=now
    #  A      ~ ?
    #  B  X  Y
    # sync(B->A): Do nothing
    # Client: B 	
    #			modVector: {"B":2} # By optimization {"B":2} implies {"B":1, "B":2} 
    #			syncVector: {} 
    # Server: A
    #			modVector: {}
    #			syncVector: {}
    ###### CODE
    #request_file.file = 'Dir1/blah.txt'
    #modVector1 = request_file.modVector.add()
    #modVector1.replica = "B"
    #modVector1.timestamp = 1
    #modVector2 = request_file.modVector.add()
    #modVector2.replica = "B"
    #modVector2.timestamp = 2
    #syncVector1 = request_file.syncVector.add()
    #syncVector1.replica = "B"
    #syncVector1.timestamp = 3

    response = stub.sync(sync_request)
    print "tra test client received ack = {} from replica {}: ".format(response.ack, response.replica_id)

if __name__ == '__main__':
    test_local_client()
