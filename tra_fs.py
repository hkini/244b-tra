'''
This file contains tra's interface code with a folder on disk.
Also contains a method for creating a UUID for each server/client - we 
can use this to uniquely identify a replica.
'''

import os
import pickle
from tra_core import traCore
from util import traVectorPair
from util import print_vector, max_vector
import constant
import util
import platform
import uuid
import time


ROOT_PATH = 'TEST_DIR'

TRACORE_FILE = '.tracore_pickle'
UUID_FILE = constant.UUID_FILE
TRACORE_CONFLICT_SUFFIX = '.tra_conflict'

NOSYNC_FILE_SUFFIXES = [TRACORE_FILE, UUID_FILE, TRACORE_CONFLICT_SUFFIX]

def create_new_dir_no_parent(abs_dir):
    if os.path.exists(abs_dir) and os.path.isdir(abs_dir):
        print abs_dir
        raise "Attempt to create existed directory {}".format(abs_dir)
    if constant.VERBOSE: print "Creating fresh tra core {}".format(abs_dir)
    os.mkdir(abs_dir)
    parent, base = os.path.split(abs_dir)
    return create_tracore_from_client_path(parent, base)


def create_new_dir_tracore(parent_core, dir_name):
    abs_dir = os.path.join(parent_core.absolute_full_path, dir_name)
    if constant.VERBOSE: print abs_dir
    if os.path.exists(abs_dir) and os.path.isdir(abs_dir):
        print abs_dir
        raise "Attempt to creat existed directory {}".format(abs_dir)
    os.makedirs(abs_dir)
    new_core = create_tracore_from_path(parent_core.absolute_full_path, dir_name)
    parent_core.add_child(new_core)
    new_core.set_parent(parent_core)
    return new_core

# Ensures that this file should be synced (i.e. is not a special file)
# based on filename.
def ensure_syncable_file(path):
    for extension in NOSYNC_FILE_SUFFIXES:
        if path.endswith(extension) or path == extension or path.startswith('.') or extension in path:
            return False
    return True

# This is taken from stackoverflow: 
# https://stackoverflow.com/questions/237079/how-to-get-file-creation-modification-date-times-in-python
# ctime in Unix is the last time modified. So have to use os.stat to get creation time.
def get_creation_time(path_to_file):
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            return stat.st_mtime

# Creates a tracore based on a directory on disk.
# Takes the absolute path of the directory's parent directory, and the
# directory name itself.
# This should be the ONLY place you create a tracore.
def create_tracore_from_path(parent_dir_abs_path, tracore_dir_name):
    if constant.VERBOSE: print "Creating core from {}".format(tracore_dir_name)
    tracore_dir_full_path = os.path.join(parent_dir_abs_path, tracore_dir_name)
    ctime = int(get_creation_time(tracore_dir_full_path))
    mtime = int(os.path.getmtime(tracore_dir_full_path))
    tracore = traCore(tracore_dir_name, replicaID=util.get_uuid(), absolute_full_path=tracore_dir_full_path)
    tracore.vector_pair.creationVector[tracore.replicaID] = ctime
    tracore.vector_pair.syncVector[tracore.replicaID] = mtime
    tracore.vector_pair.modVector[tracore.replicaID] = mtime
    files = [f for f in os.listdir(tracore_dir_full_path) if os.path.isfile(os.path.join(tracore_dir_full_path, f)) and f[-1] != '~' and ensure_syncable_file(f)]
    for f in files:
        ctime = int(get_creation_time(os.path.join(tracore_dir_full_path, f)))
        mtime = int(os.path.getmtime(os.path.join(tracore_dir_full_path, f)))
        tracore.files[f] = traVectorPair()
        tracore.files[f].syncVector[tracore.replicaID] = mtime
        tracore.files[f].modVector[tracore.replicaID] = mtime
        tracore.files[f].creationVector[tracore.replicaID] = ctime
        if constant.VERBOSE: print "Populating " + f + " with vectors"
        if constant.VERBOSE: print_vector(tracore.files[f])
    return tracore

# Creates a tracore based client's path. 
# Because we're taking it from client, we do not populate the vectors with our own info. 
# Need to populate the directory later after the files have synced.
# We don't need to pass in UUID because we will create empty vectors 
# (to copy over client's vectors later).
def create_tracore_from_client_path(parent_dir_abs_path, tracore_dir_name):
    if constant.VERBOSE: print "Creating core from client path {}".format(tracore_dir_name)
    tracore_dir_full_path = os.path.join(parent_dir_abs_path, tracore_dir_name)
    tracore = traCore(tracore_dir_name, replicaID=util.get_uuid(), absolute_full_path=tracore_dir_full_path)
    return tracore
    
# Given a path, loads the TraCore associated with
# the path, including subdirectories if called on a directory path.
# If the tracore for this path doesn't exist on disk, 
# we create it.
# Does NOT create tracores on disk, or update a created tracore.
def populate_tra_cores(path):
    # Make sure path is valid.
    path = os.path.expanduser(path)
    if not(os.path.exists(path)):
        if constant.VERBOSE: print "populate tra core: Path doesn't exist! " + path
        return(-1, None)
    tracore_directory_path = None
    is_file = False
    if not(os.path.isdir(path)):
        is_file = True
        tracore_directory_path = os.path.dirname(os.path.realpath(path))
    else:
        tracore_directory_path = path  

    # Grab TraCore if it exists, create if not.
    tracore_path = os.path.join(tracore_directory_path, TRACORE_FILE)
    tracore = None
    retval = 0
    if constant.VERBOSE: print("Examining " + tracore_path + " for a tracore")
    if (os.path.exists(tracore_path) and os.path.isfile(tracore_path)):
        if constant.VERBOSE: print ("loading pickled tracore from path " + tracore_path)
        if constant.VERBOSE: print ("If this isn't the directory you want to sync, or the directory of the file you want to sync,")
        if constant.VERBOSE: print ("you should debug this LOL.")
        with open(tracore_path, 'r') as pickle_file:
            tracore = pickle.load(pickle_file)
    else:
        parent_dir_abs_path, tracore_dir_name = os.path.split(os.path.abspath(tracore_directory_path))
        tracore = create_tracore_from_path(parent_dir_abs_path, tracore_dir_name)
        retval = 1

    # In case the caller requested a directory path, it's conceivable
    # they want to sync with a directory, so grab subdirectories.
    if not(is_file):
        for child_path in os.listdir(tracore_directory_path):
            joined_path = os.path.join(path, child_path)
            if os.path.isdir(joined_path):
                status, child_tracore = populate_tra_cores(path=joined_path)
                if (status >= 0):
                    tracore.add_child(child_tracore)
                    child_tracore.parent = tracore
    return (retval, tracore)

# Updates the tra_core's file modification times from the local files.
def update_tra_core_files_no_recurse(tracore, path=None):
    path = os.path.expanduser(path)
    for child_path in os.listdir(path):
        if child_path == TRACORE_FILE or not ensure_syncable_file(child_path):
            continue
        joined_path = os.path.join(path, child_path)
        if os.path.isdir(joined_path):
            pass
        elif os.path.isfile(joined_path):
            # Handle a file that got added to the directory after we last ran an update.
            if not child_path in tracore.files:
                if constant.VERBOSE: print("Couldn't find a vector time pair for " + child_path + " in " + path + " while updating")
                if constant.VERBOSE: print("Adding a vector for that file to the tracore.")
                tracore.add_file(child_path)
            # Update only if mtime has changed since last sync!
            mtime = int(os.path.getmtime(joined_path))
            if (tracore.replicaID in tracore.files[child_path].syncVector) and (mtime == tracore.files[child_path].syncVector[tracore.replicaID]):
                if constant.VERBOSE: print "This file " + joined_path + " has not changed since last sync. Do not update its vectors."
                pass
            elif mtime <= tracore.files[child_path].conflictedTimestamp:
                if constant.VERBOSE: print "Ignoring mtime " + str(mtime) + " bc same as timestamp of conflict resolution " + str(tracore.files[child_path].conflictedTimestamp)
                pass
            else: 
                if constant.VERBOSE: print "Not ignoring mtime " + str(mtime) + " bc timestamp of conflict resolution " + str(tracore.files[child_path].conflictedTimestamp)
                tracore.files[child_path].modVector[tracore.replicaID] = mtime
                if constant.VERBOSE: print "Adding sync vector for " + joined_path + " replica " + str(tracore.replicaID) + " with mtime " + str(mtime)
                tracore.files[child_path].syncVector[tracore.replicaID] = mtime
                ctime = int(get_creation_time(joined_path))
                # There should only be one creation vector entry
                if len(tracore.files[child_path].creationVector) == 0:
                    tracore.files[child_path].creationVector[tracore.replicaID] = ctime
    
    # Delete any files' vector pairs that no longer exist.
    for child_path in tracore.files.keys():
        joined_path = os.path.join(path, child_path)
        # (Page 9) "After the synchronizer has scanned a directory containing a recently 
        # deleted file, the directory and the deleted file will have the same sync 
        # time. Since the synchronization time is the only metadata associated with a 
        # deletion notice, the deletion notice (including the deleted files name!) can
        # be thrown away. If information about an unknown file is ever needed, the deletion 
        # notice can be reconstructed using the parent directorys current sync time."
        if not(os.path.exists(joined_path)) or (os.path.exists(joined_path) and not(os.path.isfile(joined_path))):
            if constant.VERBOSE: print "Found deleted file! " + joined_path
            if constant.VERBOSE: print_vector(tracore.vector_pair)
            # When the synchronizer notices that a file has been deleted on the local 
            # replica, it creates a deletion notice with the files current sync time       	
            tracore.vector_pair.syncVector[tracore.replicaID] = int(time.time())
            # mtimeup(file, now)
            tracore.vector_pair.modVector[tracore.replicaID] = int(time.time())
            if constant.VERBOSE: print "Sync vector update for dir: " + tracore.path 
            if constant.VERBOSE: print_vector(tracore.vector_pair)
            # Deletion notice needs to propagate up because syncing can happen at any
            # higher level
            if tracore.parent:
                if constant.VERBOSE: print "Parent is: " + tracore.parent.path
                if constant.VERBOSE: print_vector(tracore.parent.vector_pair)
                tracore.parent.percolate_up(tracore.parent, tracore.vector_pair.syncVector)
                tracore.parent.mtimeup_dir(tracore.parent, tracore.vector_pair.modVector)
                if constant.VERBOSE: print "New parent vector"
                if constant.VERBOSE: print_vector(tracore.parent.vector_pair)
            del tracore.files[child_path]


# Updates the tra_core's overall modification time from its local files and folders.
# Updates for deleted files/folders as necessary.
# Recursively calls on subfolders.
def update_tracore_recursively(tracore, path=ROOT_PATH, sync_dirs=True):
    path = os.path.expanduser(path)
    this_directory_full_path = os.path.abspath(path) # For readability
    if not os.path.exists(this_directory_full_path):
        print("Couldn't update directory!")
    
    # 1. Recursively sync directories first.
    if sync_dirs:
        for child_path_component in os.listdir(this_directory_full_path):
            child_path_full = os.path.join(this_directory_full_path, child_path_component)
            if os.path.isdir(child_path_full):
                child_tracore = None
                if not child_path_component in tracore.child:
                    child_tracore = create_tracore_from_path(this_directory_full_path, child_path_component)
                else:
                    child_tracore = tracore.child[child_path_component]
                update_tracore_recursively(child_tracore, path=child_path_full)
                tracore.add_child(child_tracore)
                child_tracore.parent = tracore

        # 2. Check for deleted directories in our TraCore (that no longer exist on disk)
        # and prune these.
        for child_dir in tracore.child.keys():
            joined_path = os.path.join(this_directory_full_path, child_dir)
            if not(os.path.exists(joined_path)):
                tracore.child[joined_path] = None

    # 3. Sync all local files.
    update_tra_core_files_no_recurse(tracore, this_directory_full_path)

    # 4. Update directory time vectors.
    # 4.1 Creation time is just the directory's creation time.
    if len(tracore.vector_pair.creationVector) == 0 and tracore.vector_pair.modVector: # Update only if directory is created on server => has a modVector
        ctime = int(get_creation_time(this_directory_full_path))    
        tracore.vector_pair.creationVector[tracore.replicaID] = ctime

    # 4.2
    # Mod time for a core (directory) is the max of the mod time of all
    # its children.
    # Sync time for a core (directory) is the min of the sync time of all
    # its children.
    #if tracore.replicaID in tracore.vector_pair.modVector:
    #    mod_time = tracore.vector_pair.modVector[tracore.replicaID]
    #if tracore.replicaID in tracore.vector_pair.syncVector:
    #    sync_time = tracore.vector_pair.syncVector[tracore.replicaID]

    prev_child_sync_vector = {}
    prev_child_mod_vector = {}
    min_sync_vectors = {}
    
    for child_path_component in os.listdir(this_directory_full_path):
        if not(ensure_syncable_file(child_path_component)):
            continue
        child_path_full = os.path.join(this_directory_full_path, child_path_component)
        child_vector_pair = None
        if os.path.isdir(child_path_full):
            if sync_dirs == False:
                continue
            child_vector_pair = tracore.child[child_path_component].vector_pair
        elif os.path.isfile(child_path_full):
            child_vector_pair = tracore.files[child_path_component]
        # Take the union
        tracore.vector_pair.modVector = max_vector(tracore.vector_pair.modVector, child_vector_pair.modVector)
        # Take the intersection (min element-wise) of children
        if not prev_child_sync_vector:
            prev_child_sync_vector = child_vector_pair.syncVector
        else: 
            intersection = set(prev_child_sync_vector.keys()) & set(child_vector_pair.syncVector.keys())
            for key in intersection:
                prev_child_sync_vector[key] = min(child_vector_pair.syncVector[key], prev_child_sync_vector[key])
    
    # Now compare children's sync vectors with parent's
    # taking the more up to date value. Even if some children have not been
    # synced, if the parent's directory has a latter timestamp => deletion notice,
    # then parent's directory need reflect that it has been synced since deletion because
    # the next sync to parent's directory again should do nothing (if nothing has been changed
    # on client since deletion). If parent's directory does not keep the deletion notice,
    # the next sync to the parent's directory would detect the deletion notice again and
    # try to recurse down the parent's directory.   
    for key in prev_child_sync_vector.keys():
        # Skip this key if the parent already has a more up-to-date timestamp
        if key in tracore.vector_pair.syncVector and tracore.vector_pair.syncVector[key] > prev_child_sync_vector[key]:
            pass
        else:
            tracore.vector_pair.syncVector[key] = prev_child_sync_vector[key]

    return tracore

# If find_core fails, we can use this to find a tracore on disk, maybe!
def find_core_resolve_path_helper(path):
    if constant.VERBOSE: print("Couldn't find path " + path + " within parent. Trying to find it absolutely...")
    path = os.path.expanduser(path)
    path = os.path.abspath(path)
    if constant.VERBOSE: print("Checking at this path: " + path)
    if not(os.path.exists(path)):
        # Check if maybe the parent directory exists, assuming this is a file. TODO should handle this cleaner.
        if os.path.exists(os.path.dirname(path)):
            retval, tracore = populate_tra_cores(path=os.path.dirname(path))
            return tracore
        return None
    else:
        retval, tracore = populate_tra_cores(path=path)
        if (retval == 0):
            if constant.VERBOSE: print("Found an existing tracore for path " + path)
        elif retval == 1:
            if constant.VERBOSE: print("Created a new tracore for path "+ path)
        else:
            return None
        return tracore

# Given a directory path and a core corresponding to that path,
# dumps the traCore to the path. Recursively dumps child traCores by default.
def dump_tra_cores(tracore, path=ROOT_PATH, recursively_dump=True):
    # Make sure path is valid.
    if not(os.path.exists(path)):
        if os.path.exists(os.path.expanduser(path)):
            path = os.path.expanduser(path)
        else:
            print "Path doesn't exist!"
            return -1
    if not(os.path.isdir(path)):
        print "This path is not a path to a directory! " + path
        return -2
    
    # Dump the tracore for the directory we're in.
    tracore_path = os.path.join(path, TRACORE_FILE)
    if constant.VERBOSE: print("dumping tracore-path " + tracore_path)
    with open(tracore_path, 'w') as pickle_file:
        # Don't dump children here - they'll be dumped in their own folders.
        temp_tracore_child = tracore.child
        tracore.child = {}
        pickle.dump(tracore, pickle_file)
        tracore.child = temp_tracore_child
    
    # Recursively dump children directories' tracores, if asked to.
    if (recursively_dump):
        for child_path_component in tracore.child.keys():
            dump_tra_cores(tracore.child[child_path_component], 
                os.path.join(path, child_path_component))

    return 0

# Returns a generator that spits out 1024 bytes of the file at a time, along with a status.
def read_file_from_path(path):
    file_path = os.path.join(ROOT_PATH, path)
    if constant.VERBOSE: print("Checking for file to download in " + file_path) 
    if os.path.exists(file_path):
        with open(file_path, 'rb') as f:
            message = f.read(1024)
            status = len(message)
            while status > 0:
                yield status, message
                message = f.read(1024)
                status = len(message)
        yield status, message
    else:
        yield -1, ""

