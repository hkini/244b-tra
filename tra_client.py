from tra_core import traCore
import trasrv_pb2_grpc
import constant
from constant import *
from trasrv_pb2 import syncRequest, fileChunkUpload, rsyncStartReply, startRsyncRequest, rsyncYieldUpload, overwriteReply, overwriteRequest
import tra_core
import os
import time
import tra_fs
import hashlib, zlib
import util
from util import setVector, updateVectorForReplica, print_vector, max_vector, traVectorPair, create_traVectorPair

sync_file_count = 0
sync_dir_count = 0
'''
This file contains client job to sync a local directory to a remote replica
syncClientJob assumes all tra vector is updated according to changes already
'''
class RsyncLookupTable(object):
    def __init__(self, checksums):
        self.dict = {}
        i = 0
        for checksum in checksums:
            if checksum.week not in self.dict:
                self.dict[checksum.week] = dict()
            self.dict[checksum.week][checksum.strong] = i
            i += 1

    def __getitem__(self, blockdata):
        week = zlib.adler32(blockdata)
        if week in self.dict:
            strong = hashlib.md5(blockdata).digest()
            return self.dict[week].get(strong)
        return None


class stubRunner:
    def __init__(self, channel):
        self.stub = trasrv_pb2_grpc.traServerStub(channel)

    def overwrite_mtime(self, job_id, remote_path):
        request = overwriteRequest(id=job_id, file_path=remote_path)
        reply = self.stub.overwrite_mtime(request)
        if reply.status == False:
            raise "BUG in overwrite_mtime"

    def upload_file(self, local_path, remote_path):
        abs_local_path = os.path.expanduser(local_path)
        if os.path.exists(abs_local_path) and os.path.isfile(abs_local_path):
            if os.path.getsize(abs_local_path) > RSYNC_THRESHOLD:
                if self.upload_file_rsync(local_path, remote_path) == 0:
                    # fallback to raw copying if remote file doesn't exist
                    self.upload_file_raw(local_path, remote_path)
            else:
                self.upload_file_raw(local_path, remote_path)
        else:
            print "ERROR: upload file {} does not exist or not a file".format(local_path)
            raise "file upload error"


    def upload_file_raw(self, local_path, remote_path):
        if (constant.VERBOSE): print "uploading file raw from {} to {}".format(local_path, remote_path)
        def upload_generator(local_path, remote_path):
            for status, bytes in tra_fs.read_file_from_path(local_path):
                if (constant.VERBOSE): print("Sending some bytes over! Status: " + str(status))
                yield fileChunkUpload(path = remote_path, status = status, message = bytes)
                
        self.stub.upload(upload_generator(local_path, remote_path))

    def upload_file_rsync(self, local_path, remote_path, mtime):
        if (constant.VERBOSE): print "uploading file rsync from {} to {}".format(local_path, remote_path)
        request = startRsyncRequest(file_path=remote_path)
        reply = self.stub.start_rsync(request)
        if reply.id > 0:
            def chunk_generator(local_path, checksums, id):
                local_f = open(os.path.expanduser(local_path), 'r')
                table = RsyncLookupTable(checksums)
                block_data = local_f.read(RSYNC_BLOCK_SIZE)
                count = 0
                unmatched = 0
                while block_data:
                    block_number = table[block_data]
                    if block_number:
                        yield rsyncYieldUpload(id=id,
                                               status=1,
                                               offset=block_number * RSYNC_BLOCK_SIZE,
                                               length=len(block_data))
                        block_data = local_f.read(RSYNC_BLOCK_SIZE)
                        count += 1
                    else:
                        yield rsyncYieldUpload(id=id,
                                               status=1,
                                               byte=block_data[0])
                        block_data = block_data[1:] + local_f.read(1)
                        unmatched += 1
                yield rsyncYieldUpload(id=id, status=0)
                if constant.VERBOSE: print "RSYNC: {} synced. Found {} matching block, {} unmatched bytes".format(local_path, count, unmatched)
            self.stub.rsync_upload(chunk_generator(local_path, reply.checksums, reply.id))
            return 1
        else:
            return 0
            #if constant.VERBOSE: print "done with rsyncing"
            #local_f.close()


    def sync_dir(self, path, vector, job_id=-1):
        global sync_dir_count
        sync_dir_count = sync_dir_count + 1
        print("sync_dir_count: " + str(sync_dir_count))

        if constant.VERBOSE: print "TRA_CLIENT: sending sync request for dir {}".format(path)
        request = syncRequest(type=syncRequestType.REQUEST_DIR.value, replica_id=util.get_uuid(), job_id=job_id, dir = path)
        request.dir = path
        for r, t in vector.modVector.iteritems():
            single = request.modVector.add()
            single.replica = r
            single.timestamp = t
        for r, t in vector.syncVector.iteritems():
            single = request.syncVector.add()
            single.replica = r
            single.timestamp = t
        for r, t in vector.creationVector.iteritems():
            single = request.creationVector
            single.replica = r
            single.timestamp = t
        reply = self.stub.sync(request)
        if constant.VERBOSE: print "TRA_CLIENT: got sync result {}".format(reply.results)
        return reply.job_id, reply.results

    def sync_files(self, path, file_vectors, job_id):
        global sync_file_count
        sync_file_count = sync_file_count + 1
        print("sync_file_count: " + str(sync_file_count))

        request = syncRequest(type=syncRequestType.REQUEST_FILE.value, replica_id=util.get_uuid(), job_id=job_id, dir=path)
        files = []
        for f, vectors in file_vectors.iteritems():
            file_vector = request.vectors.add()
            file_vector.file = f
            files.append(f)
            for r, t in vectors.modVector.iteritems():
                single = file_vector.modVector.add()
                single.replica = r
                single.timestamp = int(t)
            for r, t in vectors.syncVector.iteritems():
                single = file_vector.syncVector.add()
                single.replica = r
                single.timestamp = int(t)
            for r, t in vectors.creationVector.iteritems():
                single = file_vector.creationVector
                single.replica = r
                single.timestamp = int(t)
        if constant.VERBOSE: print "sending out sync request for files {}".format(files)
        reply = self.stub.sync(request)
        return reply.job_id, reply.results

    def sync_done(self, job_id):
        request = syncRequest(type=syncRequestType.REQUEST_COMPLETE.value, job_id=job_id)
        return self.stub.sync(request)


class syncClientJob:
    def __init__(self, sync_core, stub_runner, remote_base, file=None):
        self.sync_core = sync_core
        self.stub = stub_runner
        self.outstandings = [self.sync_core]
        self.remote_base = remote_base
        self.local_base = sync_core.absolute_full_path
        self.start = 1
        self.file = file

    def local2remotepath(self, local_abs_path):
        if constant.VERBOSE: print local_abs_path

        rel_path = local_abs_path[len(self.local_base)+1:]
        if constant.VERBOSE: print rel_path
        if constant.VERBOSE: print self.remote_base
        return os.path.join(self.remote_base, rel_path)

    def handle_conflict(self, file, uuid, mod_vector):
        # handle conflict by sending file_{replica id}_{last mod vector}
        last_ts = mod_vector[uuid]
        remote_path = self.local2remotepath(file) + tra_fs.TRACORE_CONFLICT_SUFFIX + "_" + str(uuid) + "_" + str(last_ts)
        self.stub.upload_file(file, remote_path)


    def run(self):
        pass

class syncClientJobDir(syncClientJob):
    def run(self):
        job_id = -1
        while len(self.outstandings) != 0:
            self.sync_core = self.outstandings.pop(0)
            if self.start:
                job_id, results = self.stub.sync_dir(self.remote_base,
                                                     self.sync_core.vector_pair, job_id=job_id)
                self.start = 0
            else:
                job_id, results = self.stub.sync_dir(self.local2remotepath(self.sync_core.absolute_full_path),
                                                     self.sync_core.vector_pair, job_id=job_id)
            if results[0] == syncResult.COPY.value:
                if self.sync_core.files:
                # sync all children and files if conflict
                    _, file_results = self.stub.sync_files(self.sync_core.path, self.sync_core.files, job_id)
                    for i, result in enumerate(file_results):
                        if constant.VERBOSE: print "got sync result {} for file {}".format(result, self.sync_core.files.keys()[i])
                        if result == syncResult.COPY.value:
                            local_file_name = self.sync_core.files.keys()[i]
                            local_abs_file = os.path.join(self.sync_core.absolute_full_path, local_file_name)
                            self.stub.upload_file(local_abs_file,
                                                  self.local2remotepath(local_abs_file))
                            self.stub.overwrite_mtime(job_id, self.local2remotepath(local_abs_file))
                            if constant.VERBOSE: print "TRA_CLIENT: copy over file {}".format(self.sync_core.files.keys()[i])
                        if result == syncResult.CONFLICT.value:
                            local_abs_file = os.path.join(self.sync_core.absolute_full_path,
                                                          self.sync_core.files.keys()[i])
                            self.handle_conflict(local_abs_file, self.sync_core.replicaID,
                                                 self.sync_core.files[self.sync_core.files.keys()[i]].modVector)
                            self.stub.overwrite_mtime(job_id, self.local2remotepath(local_abs_file))
                # push all children to outstanding queue to sync up in next iteration
                for _, child_core in self.sync_core.child.iteritems():
                    self.outstandings.append(child_core)
            elif results[0] == syncResult.FAULT.value:
                print "TRA_CLIENT: Got sync error"
                exit()
        if constant.VERBOSE: print "TRA_CLIENT: Done with syncing"
        self.stub.sync_done(job_id)

class syncClientJobFile(syncClientJob):

    def run(self):
        if not self.sync_core:
            print "TRA_CLIENT: ERROR: file dir {} not found".format(self.file)
            exit()
        if self.file not in self.sync_core.files:
            print self.sync_core.files
            print "TRA_CLIENT: ERROR: file {} not found in {}".format(self.file, self.sync_core.path)
            exit()
        single_dict = {self.file: self.sync_core.files[self.file]}
        job_id, result = self.stub.sync_files(self.remote_base, single_dict, -1)
        if result[0] == syncResult.COPY.value:
            if constant.VERBOSE: print "TRA_CLIENT: copying file {} over".format(self.file)
            self.stub.upload_file(os.path.join(self.local_base, self.file), os.path.join(self.remote_base, self.file))
        elif result[0] == syncResult.CONFLICT.value:
            local_abs_file = os.path.join(self.local_base,
                                          self.file)
            self.handle_conflict(local_abs_file, self.sync_core.replicaID,
                                 self.sync_core.files[self.file].modVector)
        elif result[0] == syncResult.FAULT.value:
            print "TRA_CLIENT: Got sync error"
            exit()
        if constant.VERBOSE: print "TRA_CLIENT: Done with syncing"
        self.stub.sync_done(job_id)

